//const MongoClient = require('mongodb').MongoClient;
const mongoose = require("mongoose");




//let agenda = null;
const uri = process.env.DB_STRING;
const options = {
    autoIndex: false, // Don't build indexes
    //reconnectTries: 30, // Retry up to 30 times
    //reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    useNewUrlParser: true,
    useUnifiedTopology: true
};

if (process.env.NODE_ENV == "development") {
    mongoose.connect(`mongodb://${host}:${port}/${dbName}`, options);
} else { //if(process.env.NODE_ENV == "staging"){
    mongoose.connect(uri, options);
}

// Retry connection
const connectWithRetry = () => {
    console.log("MongoDB connection with retry");
    if (process.env.NODE_ENV == "development") {
        return mongoose.connect(`mongodb://${host}:${port}/${dbName}`, options);
    } else {
        return mongoose.connect(uri, options);
    }

};
const myDB = mongoose.connection;

//Error handling if conncetion fails
myDB.on("error", function(err) {
    console.log("connection error", err);
    console.log("MongoDB connection unsuccessful, retry after 5 seconds.");
    setTimeout(connectWithRetry, 1000);
})

//Check if successful connection is made
myDB.once("open", function callback() {
    console.log("MY DB Connected with Mongoose");
});



module.exports = {
    myDB: myDB
};